module.exports = function (pool, pollRepo) {
  return {
      
      async create(ctx) {
          const createPoll = ctx.request.body
          console.log(createPoll)
          const getPoll = await pollRepo.create(pool, createPoll['tweet_id'])
          ctx.body = createPoll
          
      },
      
      async addChoice(ctx) {
        const createChoice = ctx.request.body
        console.log(createChoice)
        const getChoice = await pollRepo.create(pool, createChoice)
        ctx.body = createChoice
        
      },        
     
      async vote(ctx) {
        const id = ctx.params.id
        let getTodo = await pollRepo.find(pool, id)
        ctx.body = getTodo
        // TODO: validate id
        // find todo from repo
        // send todo
    },        
  }
}
